import * as React from 'react';
import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';
import Key from '../components/key';
import Display from '../components/display';
import { OPERATIONS, DIGITS } from '../constants';
import {
  insertDigit as insertDigitOperation,
  IInsertDigit,
  applyOperation as applyOperationAction,
  IApplyOperation,
  reset as resetAction,
  IReset,
} from '../actions/index';
import { TDigit, IState, TOperation } from '../types';

import './calculator.scss';
import logo from '../images/logo.png';

interface ICalculatorProps {
  insertDigit: IInsertDigit;
  applyOperation: IApplyOperation;
  reset: IReset;
  value: IState['value'];
  storedValue: IState['storedValue'];
}

export class Calculator extends React.Component<ICalculatorProps> {
  onDigitClick = (digit: TDigit) => {
    const { insertDigit } = this.props;
    insertDigit(digit);
  };
  onOperationClick = (action: TOperation) => {
    const { applyOperation } = this.props;
    applyOperation(action);
  };
  onResetClick = () => {
    const { reset } = this.props;
    reset();
  };
  render() {
    const { value, storedValue } = this.props;
    const digits = DIGITS.map(digit => (
      <li key={digit}>
        <Key text={digit} onClick={this.onDigitClick} />
      </li>
    ));

    const operations = OPERATIONS.map(operation => (
      <li key={operation}>
        <Key key={operation} text={operation} onClick={this.onOperationClick} />
      </li>
    ));

    return (
      <React.Fragment>
        <img src={logo} className="logo" alt="Equal Experts logo" />
        <Display text={value || storedValue} />
        <div className="keys">
          <ul className="keys--main">
            <li>
              <Key text="AC" onClick={this.onResetClick} />
            </li>
            {digits}
          </ul>
          <ul className="keys--operations">{operations}</ul>
        </div>
      </React.Fragment>
    );
  }
}

const mapDispatchToProps = (dispatch: Dispatch) => ({
  insertDigit: bindActionCreators(insertDigitOperation, dispatch),
  applyOperation: bindActionCreators(applyOperationAction, dispatch),
  reset: bindActionCreators(resetAction, dispatch),
});

const mapStateToProps = (state: IState) => ({
  value: state.value,
  storedValue: state.storedValue,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Calculator);

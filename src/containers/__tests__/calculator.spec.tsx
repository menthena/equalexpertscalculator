import * as React from 'react';
import { ShallowWrapper, shallow } from 'enzyme';
import { Calculator } from '../calculator';

const findKeyByText = (text: string | number, wrapper: ShallowWrapper) =>
  wrapper.findWhere(
    (element: ShallowWrapper) =>
      element.is('Key') && element.prop('text') === text,
  );
describe('Calculator', () => {
  let wrapper: ShallowWrapper;
  const props = {
    insertDigit: jest.fn(),
    applyOperation: jest.fn(),
    reset: jest.fn(),
    value: 10,
    storedValue: 10,
  };
  beforeEach(() => {
    wrapper = shallow(<Calculator {...props} />);
  });
  it('matches the snapshot', () => {
    expect(wrapper).toMatchSnapshot();
  });
  it('resets the calculator', () => {
    const resetKey = findKeyByText('AC', wrapper);
    resetKey.simulate('click');
    expect(props.reset).toBeCalled();
  });
  it('inserts digit', () => {
    const digitKey = findKeyByText(1, wrapper);
    digitKey.simulate('click');
    expect(props.insertDigit).toBeCalled();
  });
  it('inserts digit', () => {
    const operationKey = findKeyByText('/', wrapper);
    operationKey.simulate('click');
    expect(props.applyOperation).toBeCalled();
  });
  describe('Display', () => {
    it('displays storedValue when currentValue is 0', () => {
      wrapper.setProps({
        value: 0,
        storedValue: 10,
      });
      const display = wrapper.find('Display');
      expect(display.prop('text')).toBe(10);
    });
    it('displays value when it is not 0', () => {
      wrapper.setProps({
        value: 12,
        storedValue: 15,
      });
      const display = wrapper.find('Display');
      expect(display.prop('text')).toBe(12);
    });
  });
});

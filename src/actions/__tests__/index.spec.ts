import { insertDigit, applyOperation, reset } from '..';
import actionTypes from '../actionTypes';

describe('Actions', () => {
  it('insertDigit returns the right actionType and digit', () => {
    expect(insertDigit(1)).toEqual({
      type: actionTypes.INSERT_DIGIT,
      digit: 1,
    });
  });
  it('applyOperation returns the right actionType and operation', () => {
    expect(applyOperation('*')).toEqual({
      type: actionTypes.APPLY_OPERATION,
      operation: '*',
    });
  });
  it('reset returns the right actionType', () => {
    expect(reset()).toEqual({
      type: actionTypes.RESET,
    });
  });
});

import { TDigit, TOperation } from '../types/index.d';
import actionTypes from './actionTypes';
import { ActionCreator } from 'redux';

export type IInsertDigit = ActionCreator<{ type: string; digit: TDigit }>;
export const insertDigit = (digit: TDigit) => ({
  type: actionTypes.INSERT_DIGIT,
  digit,
});

export type IApplyOperation = ActionCreator<{
  type: string;
  operation: TOperation;
}>;
export const applyOperation = (operation: TOperation) => ({
  type: actionTypes.APPLY_OPERATION,
  operation,
});

export type IReset = ActionCreator<{
  type: string;
}>;
export const reset = () => ({
  type: actionTypes.RESET,
});

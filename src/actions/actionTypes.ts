const actionTypes = {
  INSERT_DIGIT: 'INSERT_DIGIT',
  APPLY_OPERATION: 'APPLY_OPERATION',
  RESET: 'RESET',
};

export default actionTypes;

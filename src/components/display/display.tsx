import * as React from 'react';
import './display.scss';

interface IDisplayProps {
  text: number;
}

const Display: React.StatelessComponent<IDisplayProps> = ({ text }) => (
  <div className="display">{text}</div>
);

export default Display;

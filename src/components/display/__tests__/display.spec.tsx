import { shallow } from 'enzyme';
import * as React from 'react';
import Display from '../display';

describe('Display', () => {
  const wrapper = shallow(<Display text={123} />);
  it('displays the number', () => {
    expect(wrapper.is('.display')).toBe(true);
    expect(wrapper.text()).toBe('123');
  });
});

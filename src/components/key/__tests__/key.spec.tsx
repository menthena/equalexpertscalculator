import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';
import Key from '../key';

describe('Key', () => {
  const onClick = jest.fn();
  let wrapper: ShallowWrapper;
  beforeEach(() => {
    wrapper = shallow(<Key text="/" onClick={onClick} />);
  });
  it('returns a button with a text', () => {
    expect(wrapper.is('button')).toBe(true);
    expect(wrapper.text()).toBe('/');
  });

  it('calls onClick with passed text', () => {
    wrapper.simulate('click');
    expect(onClick).toBeCalledWith('/');
  });
});

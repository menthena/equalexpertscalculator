import { reset, insertDigit, applyOperation } from '../../actions';
import reducer, { getDefaultState } from '../reducer';
import { IState } from '../../types';
import reduceInsertDigit from '../helpers/insertDigit';
import reduceApplyOperation from '../helpers/applyOperation';

jest.mock('../helpers/insertDigit');
jest.mock('../helpers/applyOperation');

describe('reducer', () => {
  it('resets the state', () => {
    const action = reset();
    const state: IState = {
      value: 5,
      storedValue: 5,
      operation: '/',
    };
    expect(reducer(state, action)).toEqual(getDefaultState());
  });
  it('inserts digit', () => {
    (reduceInsertDigit as jest.Mock).mockReturnValue(15);
    const action = insertDigit(1);
    const state: IState = {
      value: 5,
      storedValue: 5,
      operation: '/',
    };
    expect(reducer(state, action)).toEqual({
      value: 15,
      storedValue: 5,
      operation: '/',
    });
  });
  it('applies operation', () => {
    (reduceApplyOperation as jest.Mock).mockReturnValue(10);
    const action = applyOperation('+');
    const state: IState = {
      value: 5,
      storedValue: 5,
      operation: '+',
    };
    expect(reducer(state, action)).toEqual({
      value: 0,
      storedValue: 10,
      operation: '+',
    });
  });
});

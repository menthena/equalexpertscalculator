import applyOperation from '../applyOperation';

describe('applyOperation', () => {
  it('applies addition operation', () => {
    expect(applyOperation('+', 0, 1)).toBe(1);
    expect(applyOperation('+', 5, 1)).toBe(6);
  });
  it('applies multiplication operation', () => {
    expect(applyOperation('*', 0, 1)).toBe(0);
    expect(applyOperation('*', 5, 1)).toBe(5);
  });
  it('applies division operation', () => {
    expect(applyOperation('/', 0, 1)).toBe(Infinity);
    expect(applyOperation('/', 5, 5)).toBe(1);
  });
  it('applies substraction operation', () => {
    expect(applyOperation('-', 0, 1)).toBe(1);
    expect(applyOperation('-', 5, 5)).toBe(0);
  });
  it('returns the result', () => {
    expect(applyOperation('=', 0, 1)).toBe(1);
  });
  it('returns the value when storedValue is 0', () => {
    expect(applyOperation('=', 5, 0)).toBe(5);
  });
});

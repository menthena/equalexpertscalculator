import insertDigit from '../insertDigit';

describe('insertDigit', () => {
  it('returns the digit when the current value is 0', () => {
    expect(insertDigit(0, 1)).toBe(1);
  });
  it('adds to the digit when the current value is not 0', () => {
    expect(insertDigit(1, 2)).toBe(12);
  });
});

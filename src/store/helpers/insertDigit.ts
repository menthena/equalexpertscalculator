import { TDigit, IState } from '../../types';

const reduceInsertDigit = (
  value: IState['value'],
  digit: TDigit,
): IState['value'] =>
  value === 0 ? digit : (Number(`${value}${digit}`) as IState['value']);

export default reduceInsertDigit;

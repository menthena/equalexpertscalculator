import { IState } from '../../types';

const reduceApplyOperation = (
  operation: IState['operation'],
  value: IState['value'],
  storedValue: IState['storedValue'],
): IState['storedValue'] => {
  if (storedValue === 0) {
    return value;
  }
  switch (operation) {
    case '+':
      return storedValue + value;
    case '*':
      return storedValue * value;
    case '/':
      return storedValue / value;
    case '-':
      return storedValue - value;
    default:
      return storedValue;
  }
};

export default reduceApplyOperation;

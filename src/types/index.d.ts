export type TDigit = 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 0;
export type TOperation = '/' | '+' | '-' | '*' | '=';
export type TAction = 'AC';

export interface IState {
  storedValue: number;
  value: number;
  operation: TOperation;
}
